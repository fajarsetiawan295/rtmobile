import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  YellowBox,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {FlatGrid} from 'react-native-super-grid';
import Icon from './../../component/icons';
import {Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged} from './../../redux/actions';
import {api, url} from './../../conf/url';
import {Loading} from './../../component';
import icons from './../../component/icons';

YellowBox.ignoreWarnings([
  'Calling `getNode()` on the ref of an Animated component is no longer necessary. You can now directly use the ref instead.',
  'VirtualizedLists should never be nested',
]);

class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headItem: [],
      headItemdua: [],
      animating: false,
      informasi: [],
      refresh: false,
    };
  }

  onGetProfile() {
    this.setState({animating: true});
    Get_services(this.props.token, api.Profile).then(response => {
      if (response.status == 200) {
        this.props.dataUser(response.data.data);
        this.funIcon();
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }
  onGetInformasi() {
    Get_services(this.props.token, api.AllCategory).then(response => {
      console.log('ini', response);
      if (response.status == 200) {
        this.setState({informasi: response.data.data});
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  UNSAFE_componentWillMount() {
    this.onRequestAll();
  }

  onRequestAll() {
    this.onGetProfile();
    this.onGetInformasi();
  }
  refreshContent() {
    this.setState({refresh: true});
    this.onRequestAll();
    this.setState({refresh: false});
  }

  funIcon() {
    if (
      this.props.infoUser.profile == null
        ? ''
        : this.props.infoUser.profile.role == 'RT'
    ) {
      this.setState({
        headItem: [
          {
            item: 'Informasi',
            name: 'info-circle',
            navigate: 'CreateInformasi',
            iconType: Icon.FontAwesome,
          },
          {
            item: 'Warga',
            name: 'account-convert',
            navigate: 'Warga',
            iconType: Icon.MaterialCommunityIcons,
          },
          {
            item: 'Pengaduan',
            name: 'help-network',
            navigate: 'ListPengaduan',
            iconType: Icon.MaterialCommunityIcons,
          },
          {
            item: 'Surat',
            name: 'qrcode',
            navigate: 'ListPengajuanSurat',
            iconType: Icon.FontAwesome,
          },
        ],
      });
    } else {
      this.setState({
        headItem: [
          {
            item: 'Pengaduan',
            name: 'sound',
            navigate: 'ListPengaduan',
            iconType: Icon.AntDesign,
          },
          {
            item: 'Warga Aktif',
            name: 'info-circle',
            navigate: 'ActiveWarga',
            iconType: Icon.FontAwesome,
          },
          {
            item: 'Warga Nonaktif',
            name: 'arrow-swap',
            navigate: 'NonActiveWarga',
            iconType: Icon.Fontisto,
          },
          {
            item: 'Pengajuan Surat',
            name: 'documents',
            navigate: 'ListPengajuanSurat',
            iconType: Icon.Ionicons,
          },
        ],
      });
    }
    this.setState({animating: false});
  }

  render() {
    const {navigation} = this.props;
    const img = require('./../../assets/image/test.png');
    const List = ({onPress, category, data}) => {
      return (
        <View style={styles.containercard}>
          {data.length != 0 ? (
            <Text style={styles.textcategory}>{category}</Text>
          ) : null}
          <View style={styles.subcontainercard}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {data.map((item, key) => {
                return (
                  <TouchableOpacity style={styles.card}>
                    <Image
                      source={{
                        uri: url + item.foto,
                      }}
                      style={styles.imgcard}
                    />
                    <Text style={styles.judulcard}>{item.judul_informasi}</Text>
                    <TouchableOpacity
                      style={styles.iconcomment}
                      onPress={() =>
                        this.props.navigation.navigate('CommentInformasi', {
                          id: item.id,
                        })
                      }>
                      <Icon.Octicons
                        name={'comment-discussion'}
                        color={colors.colorssatu}
                        size={wp('6%')}
                      />
                    </TouchableOpacity>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      );
    };
    return (
      // header
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading />
          </View>
        ) : (
          <View style={styles.container}>
            <View
              style={{
                height: hp('15%'),
                width: wp('100%'),
                borderBottomRightRadius: hp('25%'),
                backgroundColor: colors.colorstiga,
                flexDirection: 'row',
              }}>
              {this.props.infoUser.profile.foto == null ? (
                <Image
                  source={{
                    uri:
                      'https://png.pngtree.com/element_our/png_detail/20181102/avatar-profile-logo-vector-emblem-illustration-modern-illustration-png_227486.jpg',
                  }}
                  style={styles.imageProfile}
                />
              ) : (
                <Image
                  source={{
                    uri: url + this.props.infoUser.profile.foto,
                  }}
                  style={styles.imageProfile}
                />
              )}
              <View>
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    marginTop: hp('3%'),
                    fontSize: wp('5%'),
                    fontWeight: 'bold',
                  }}>
                  {this.props.infoUser.name}
                </Text>
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    // marginTop: hp('1%'),
                    // fontWeight: 'bold',
                    fontSize: wp('4%'),
                  }}>
                  {this.props.infoUser.profile.role}
                </Text>
              </View>
            </View>

            <View style={styles.containerIconGrid}>
              <View style={styles.subContainerIconGrid}>
                {this.state.headItem.map((item, key) => {
                  return (
                    <TouchableOpacity
                      onPress={() => navigation.navigate(item.navigate)}
                      style={styles.componentGridIcon}
                      key={key}>
                      <View style={styles.subComponentGridCyrcle}>
                        <item.iconType
                          name={item.name}
                          color={colors.colorssatu}
                          size={wp('6%')}
                        />
                      </View>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={styles.subComponentGridText}>
                        {item.item}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>

            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refresh}
                  onRefresh={() => this.refreshContent()}
                />
              }
              showsVerticalScrollIndicator={false}>
              {this.state.informasi.map((item, key) => {
                return <List category={item.judul} data={item.informasi} />;
              })}
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.YellowButton,
    alignItems: 'center',
  },
  imageProfile: {
    marginTop: hp('1%'),
    marginLeft: wp('2%'),
    width: wp('20'),
    height: wp('20'),
    borderRadius: wp('20'),
    borderWidth: 1,
  },
  containerIconGrid: {
    width: wp('95%'),
    backgroundColor: colors.colorstiga,
    borderRadius: 10,
    borderTopLeftRadius: wp('10%'),
    borderTopRightRadius: wp('10%'),
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  containerIconGriddua: {
    width: wp('95%'),
    backgroundColor: colors.colorstiga,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: hp('1%'),
  },
  subContainerIconGrid: {
    margin: wp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp('90%'),
  },
  componentGridIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('20%'),
  },
  subComponentGridCyrcle: {
    borderRadius: wp('20%'),
    backgroundColor: 'white',
    height: wp('15%'),
    width: wp('15%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  subComponentGridText: {
    marginTop: 1,
    fontFamily: 'ProximaNova',
    fontSize: wp('3%'),
    color: colors.colorssatu,
    width: wp('22%'),
    alignItems: 'center',
    textAlign: 'center',
  },
  containercard: {
    width: wp('90%'),
    marginTop: hp('1%'),
  },
  textcategory: {
    fontSize: wp('5%'),
    fontWeight: 'bold',
  },
  subcontainercard: {
    width: wp('90%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  card: {
    marginLeft: wp('2%'),
    marginTop: hp('2%'),
    width: wp('45%'),
    height: hp('35%'),
    borderRadius: wp('3%'),
    borderColor: colors.colorstiga,
    borderWidth: 1,
    backgroundColor: 'white',
  },
  imgcard: {
    width: wp('45%'),
    height: wp('40%'),
    borderTopLeftRadius: wp('3%'),
    borderTopRightRadius: wp('3%'),
    borderWidth: 1,
  },
  judulcard: {
    marginLeft: wp('2%'),
    marginTop: hp('1%'),
    fontWeight: 'bold',
  },
  iconcomment: {
    marginTop: hp('3%'),
    width: wp('40%'),
    alignItems: 'flex-end',
  },
});

// export default home;
const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
      isLogged,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(home);
