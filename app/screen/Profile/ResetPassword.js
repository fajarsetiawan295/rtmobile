import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Icon, Header} from './../../component';
import colors from '../../conf/color.global';
import {post_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import DropdownAlert from 'react-native-dropdownalert';
import {api, url} from './../../conf/url';

const Field = ({title, onChangeText, valuedata, sec}) => {
  return (
    <View style={{width: wp('80'), marginTop: hp('2')}}>
      <Text style={styles.contentTextKey}>{title}</Text>
      <TextInput
        value={valuedata}
        style={styles.textInputField}
        onChangeText={onChangeText}
        secureTextEntry={sec}
      />
    </View>
  );
};

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: this.props.dataUser == null ? '' : this.props.dataUser.email,
      old_password: '',
      password_confirmation: '',
      password: '',
    };
  }

  onAddAddress() {
    const state = this.state;
    const data = new FormData();

    console.log('ini type', state.type);
    data.append('email', state.email);
    data.append('old_password', state.old_password);
    data.append('password_confirmation', state.password_confirmation);
    data.append('password', state.password);

    post_services(this.props.token, data, api.ResetPassword)
      .then(response => {
        if (response.status == 200) {
          this.dropDownAlertRef.alertWithType(
            'success',
            'Sukses',
            'Berhasil Update Password',
          );
          setTimeout(() => {
            this.props.navigation.goBack();
          }, 7000);
          console.log(response, 'Sukkses');
        } else {
          if (response.data.message == 'Unauthenticated.') {
            this.props.isLogged(false);
            this.dropDownAlertRef.alertWithType(
              'error',
              'Error',
              'Silahkan login untuk melanjutkan',
            );
          } else {
            console.log(response, 'failed');
            var arr = response.data.message;
            var message = arr.split(',');
            this.dropDownAlertRef.alertWithType('error', 'Error', message[0]);
          }
        }
      })
      .catch(err => {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          'Terjadi Kesalahan',
        );
      });
  }

  render() {
    const {navigation} = this.props;
    const state = this.state;
    return (
      <View style={styles.container}>
        <Header
          style={{height: hp('7')}}
          onPress={() => navigation.goBack()}
          title={'Resset Password'}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Field
            title="Email"
            valuedata={this.state.email}
            sec={false}
            onChangeText={value => this.setState({email: value})}
          />
          <Field
            title="Old password"
            valuedata={this.state.old_password}
            sec={true}
            onChangeText={value => this.setState({old_password: value})}
          />
          <Field
            title="password"
            valuedata={this.state.password}
            sec={true}
            onChangeText={value => this.setState({password: value})}
          />
          <Field
            title="password Confirmation"
            valuedata={this.state.password_confirmation}
            sec={true}
            onChangeText={value =>
              this.setState({password_confirmation: value})
            }
          />

          <TouchableOpacity
            style={styles.containerContentTransfer}
            onPress={() => this.onAddAddress()}>
            <Text style={[styles.titleItem]}>Kirim</Text>
          </TouchableOpacity>
        </ScrollView>

        <DropdownAlert ref={ref => (this.dropDownAlertRef = ref)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  map: {
    flex: 1,
    width: wp('80'),
    height: hp('50'),
  },
  button: {
    margin: 8,
  },
  contentTextKey: {
    color: colors.colorsdua,
  },
  textInputField: {
    width: wp('80'),
    borderRadius: 5,
    borderColor: colors.colorsdua,
    borderWidth: 0.5,
    height: hp('7%'),
    borderBottomWidth: null,
    paddingLeft: 6,
  },
  containerContentTransfer: {
    width: wp('80'),
    backgroundColor: colors.colorsdua,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    marginVertical: hp('5'),
  },
  titleItem: {
    color: colors.white,
    marginVertical: hp('1'),
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResetPassword);
