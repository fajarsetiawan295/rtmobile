import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {navigation} = this.props;
    const List = ({onPress, title, IconLeft}) => {
      return (
        <TouchableOpacity style={styles.containerAddress} onPress={onPress}>
          <View style={styles.subContainerAddress}>
            {IconLeft}
            <Text style={styles.textList}>{title}</Text>
            <Icon.AntDesign name="rightcircle" color="#727C8E" size={24} />
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <Header onPress={() => navigation.goBack()} title="Edit User" />
        <View style={{marginTop: hp('2%')}} />
        <List
          title={'Cek Active Warga'}
          onPress={() => navigation.navigate('ActiveWarga')}
          IconLeft={
            <Icon.MaterialCommunityIcons
              name="google-maps"
              color="#949494"
              size={24}
            />
          }
        />
        <List
          title={'Cek Non Active Warga'}
          onPress={() => navigation.navigate('NonActiveWarga')}
          IconLeft={
            <Icon.MaterialCommunityIcons
              name="google-maps"
              color="#949494"
              size={24}
            />
          }
        />
        {this.props.infoUser.profile == null ? (
          ''
        ) : this.props.infoUser.profile.role == 'RT' ? (
          <>
            <List
              title={'Update Warga Pindah'}
              onPress={() => navigation.navigate('EditWarga')}
              IconLeft={
                <Icon.MaterialCommunityIcons
                  name="google-maps"
                  color="#949494"
                  size={24}
                />
              }
            />
          </>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: hp('5%'),
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('1%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    color: '#515C6F',
  },
  buttonLogOut: {
    marginTop: wp('5%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90%'),
    marginBottom: hp('5'),
    backgroundColor: 'white',
  },
  textLogOut: {
    marginVertical: hp('2%'),
    fontFamily: 'ProximaNovaSemiBold',
    color: '#1C1819',
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
