import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Clipboard,
  TouchableOpacity,
} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import {api, url} from './../../conf/url';
import {Get_services} from './../../services';
import LottieView from 'lottie-react-native';
import DropdownAlert from 'react-native-dropdownalert';
import Modal from 'react-native-modal';

class ActiveWarga extends Component {
  constructor(props) {
    super(props);
    this.state = {
      warga: [],
      keywarga: {},
      statusrt: false,
      mes: '',
      urlimg: '',
      isModalVisible: false,
    };
  }
  onActiveWarga() {
    Get_services(this.props.token, api.ActiveWarga).then(response => {
      if (response.status == 200) {
        this.setState({keywarga: response.data.data});
        this.setState({warga: response.data.data.warga});
        console.log('ini user', this.state.warga);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        this.setState({statusrt: true, mes: response.data.message});
        console.log(response, 'profile');
      }
    });
  }

  onRenderFilterDate() {
    const state = this.state;
    return (
      <Modal isVisible={this.state.isModalVisible}>
        <View
          style={{
            backgroundColor: '#ffffff',
            borderRadius: 6,
            alignItems: 'center',

            height: hp('90%'),
          }}>
          <TouchableOpacity
            onPress={() => this.setState({isModalVisible: false})}
            style={{width: wp('90%'), padding: wp('2%')}}>
            <Icon.AntDesign
              name={'closecircle'}
              color={colors.colorssatu}
              size={wp('6%')}
            />
          </TouchableOpacity>
          {console.log(this.state.urlimg)}
          <Image
            source={{
              uri: this.state.urlimg,
            }}
            style={styles.imgcard}
          />
        </View>
      </Modal>
    );
  }
  imgj(x) {
    console.log(x);
    this.setState({urlimg: x, isModalVisible: true});
  }

  UNSAFE_componentWillMount() {
    this.onActiveWarga();
  }
  thiscopy(x) {
    Clipboard.setString(x);
    this.dropDownAlertRef.alertWithType(
      'success',
      'success',
      'Berhasil copy key',
    );
  }
  render() {
    const {navigation} = this.props;
    const List = ({onPress, title, email, hp, img}) => {
      return (
        <TouchableOpacity
          style={styles.containerAddress}
          onPress={() => this.imgj(img)}>
          <View style={styles.subContainerAddress}>
            <Text>Name</Text>
            <Text style={styles.textList}>{title}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Alamat</Text>
            <Text style={styles.textList}>{email}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>No Hp</Text>
            <Text style={styles.textList}>{hp}</Text>
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <Header onPress={() => navigation.goBack()} title={'Active Warga'} />
        {this.onRenderFilterDate()}
        <TouchableOpacity
          onPress={() => this.thiscopy(this.state.keywarga.key)}
          style={{
            width: wp('95%'),
            backgroundColor: 'white',
            marginTop: hp('2%'),
          }}>
          <Text style={{fontWeight: 'bold'}}>KEY RT ANDA </Text>
          <Text style={{fontSize: 13}}>{this.state.keywarga.key}</Text>
        </TouchableOpacity>
        {this.state.statusrt == false ? (
          <>
            {this.state.warga.map((item, key) => {
              return (
                <>
                  {item.status == 'Active' ? (
                    <List
                      title={item.users.name}
                      email={item.users.profile.alamat}
                      hp={item.users.profile.nomor_hp}
                      img={url + item.users.profile.foto}
                    />
                  ) : null}
                </>
              );
            })}
          </>
        ) : (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>{this.state.mes}</Text>
            <View
              style={{
                backgroundColor: 'transparent',
                borderRadius: 10,
                width: wp('100'),
                height: wp('100'),
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <LottieView
                source={require('../../assets/lottie/empaty.json')}
                autoPlay
                loop
              />
            </View>
          </View>
        )}
        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    // fontFamily: Fonts.type.bold,
    // fontSize: Fonts.size.medium,
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('5%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    color: '#515C6F',
  },

  imgcard: {
    marginTop: hp('5%'),
    width: wp('90%'),
    height: wp('90%'),
    borderWidth: 1,
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActiveWarga);
