export {default as profile} from './Profile';
export {default as ActiveWarga} from './ActiveWarga';
export {default as NonActiveWarga} from './NonActiveWarga';
export {default as EditWarga} from './EditWarga';
export {default as Warga} from './Warga';
export {default as Password} from './ResetPassword';
