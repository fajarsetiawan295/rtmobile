import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Icon, Header} from './../../component';
import colors from '../../conf/color.global';
import {Get_services, post_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import ImagePicker from 'react-native-image-picker';
import DropdownAlert from 'react-native-dropdownalert';
import moment from 'moment';
import {api, url} from './../../conf/url';

class Help extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUpdate: '',
    };
  }

  onUserLogout() {
    this.props.tokenUser('logout');
    this.props.isLogged(false);
    this.props.navigation.navigate('Login');
  }

  onGetProfile() {
    this.setState({animating: true});
    Get_services(this.props.token, api.Profile).then(response => {
      if (response.status == 200) {
        this.props.dataUser(response.data.data);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  onPressFotoProfile() {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          name: response.fileName,
        };
        this.setState({
          imageUpdate: source,
        });
        return this.onUpdateFotoProfile();
      }
    });
  }

  onUpdateFotoProfile() {
    const data = new FormData();
    data.append('foto', this.state.imageUpdate);
    post_services(this.props.token, data, api.UpdateFoto).then(response => {
      if (response.status == 200) {
        console.log(response, 'Image');
        this.dropDownAlertRef.alertWithType(
          'success',
          'Sukses',
          response.data.message,
        );
        return this.onGetProfile();
      } else {
        this.dropDownAlertRef.alertWithType('error', 'Error', response);
        console.log(response);
      }
    });
  }

  render() {
    const {navigation} = this.props;
    const List = ({onPress, title, IconLeft}) => {
      return (
        <TouchableOpacity style={styles.containerAddress} onPress={onPress}>
          <View style={styles.subContainerAddress}>
            {IconLeft}
            <Text style={styles.textList}>{title}</Text>
            <Icon.AntDesign name="rightcircle" color="#727C8E" size={24} />
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={
              {
                // width: wp('90%'),
                // backgroundColor: 'white',
                // padding: wp('4%'),
                // justifyContent: 'center',
              }
            }>
            <Image
              source={require('../../assets/image/1.jpeg')}
              style={styles.imageProfile}
            />
            <Image
              source={require('../../assets/image/2.jpeg')}
              style={styles.imageProfile}
            />
            <Image
              source={require('../../assets/image/3.jpeg')}
              style={styles.imageProfile}
            />
            <Image
              source={require('../../assets/image/4.jpeg')}
              style={styles.imageProfile}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: hp('5%'),
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('100%'),
    height: wp('80%'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('1%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    color: '#515C6F',
  },
  buttonLogOut: {
    marginTop: wp('5%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90%'),
    marginBottom: hp('5'),
    backgroundColor: 'white',
  },
  buttonReset: {
    marginTop: wp('3%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90%'),
    backgroundColor: 'white',
  },
  textLogOut: {
    marginVertical: hp('2%'),
    fontFamily: 'ProximaNovaSemiBold',
    color: '#1C1819',
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Help);
