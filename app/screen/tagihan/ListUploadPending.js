import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Header, Loading} from './../../component';
import colors from '../../conf/color.global';
import {Get_services, post_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser} from './../../redux/actions';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import {api, url} from './../../conf/url';
import Select2 from 'react-native-select-two';
import Icon from './../../component/icons';
import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';

class ListUploadPending extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      warga: [],
      datawarga: [],
      bukti: [],
      urlimg: '',
      isModalVisible: false,
    };
  }

  OnSubmit(x) {
    this.setState({animating: true});

    const m = this.state;
    const {navigation} = this.props;
    const data = new FormData();
    data.append('upload_id', x);

    post_services(this.props.token, data, api.TagihanUserUpadte).then(
      rspons => {
        this.setState({animating: false});
        console.log(rspons);
        if (rspons.status == 200) {
          this.dropDownAlertRef.alertWithType(
            'success',
            'success',
            'Berhasil Update',
          );
          this.Onlisttagihan();
        } else if (rspons.status == 401) {
          this.props.navigation.navigate('Login');
        } else {
          this.dropDownAlertRef.alertWithType(
            'error',
            'Error',
            rspons.data.message,
          );
        }
      },
    );
  }

  Onlisttagihan() {
    this.setState({animating: true});

    Get_services(this.props.token, api.TagihanUserPending).then(rspons => {
      this.setState({animating: false});
      console.log(rspons);
      if (rspons.status == 200) {
        this.setState({bukti: rspons.data.data});
      } else if (rspons.status == 401) {
        this.props.navigation.navigate('Login');
      } else {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          rspons.data.message,
        );
      }
    });
  }
  onRenderFilterDate() {
    const state = this.state;
    return (
      <Modal isVisible={this.state.isModalVisible}>
        <View
          style={{
            backgroundColor: '#ffffff',
            borderRadius: 6,
            alignItems: 'center',

            height: hp('90%'),
          }}>
          <TouchableOpacity
            onPress={() => this.setState({isModalVisible: false})}
            style={{width: wp('90%'), padding: wp('2%')}}>
            <Icon.AntDesign
              name={'closecircle'}
              color={colors.colorssatu}
              size={wp('6%')}
            />
          </TouchableOpacity>
          <Image
            source={{
              uri: this.state.urlimg,
            }}
            style={styles.imgcard}
          />
        </View>
      </Modal>
    );
  }

  UNSAFE_componentWillMount() {
    this.Onlisttagihan();
  }

  imgj(x) {
    this.setState({urlimg: x, isModalVisible: true});
  }

  render() {
    const state = this.state;
    const {navigation} = this.props;
    const List = ({onPress, title, email, hp, tgl, img, id, usernama}) => {
      return (
        <TouchableOpacity
          style={styles.containerAddress}
          onPress={() => this.imgj(img)}>
          <View style={styles.subContainerAddress}>
            <Text>Nama Pengirim</Text>
            <Text style={styles.textList}>{usernama}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Atas Nama</Text>
            <Text style={styles.textList}>{title}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>No. Rekening</Text>
            <Text style={styles.textList}>{email}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Status</Text>
            <Text style={styles.textList}>{hp}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Tanggal</Text>
            <Text style={styles.textList}>{tgl}</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.OnSubmit(id)}
            style={styles.button}>
            <Text style={styles.textButton}>Berhasil</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      );
    };
    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading data={true} />
          </View>
        ) : (
          <View style={styles.container}>
            <Header
              onPress={() => navigation.goBack()}
              title="Upload Bukti Tagihan "
            />
            {this.onRenderFilterDate()}
            <ScrollView showsVerticalScrollIndicator={false}>
              {this.state.bukti.length != 0 ? (
                <>
                  {this.state.bukti.map((item, key) => {
                    return (
                      <>
                        <List
                          title={item.atas_nama}
                          email={item.nomor_rekening}
                          hp={item.status}
                          usernama={item.user.name}
                          tgl={moment(item.created_at).format('D MMMM YYYY')}
                          img={url + item.foto}
                          id={item.id}
                        />
                      </>
                    );
                  })}
                </>
              ) : (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: 'transparent',
                      borderRadius: 10,
                      width: wp('100'),
                      height: wp('100'),
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <LottieView
                      source={require('../../assets/lottie/empaty.json')}
                      autoPlay
                      loop
                    />
                  </View>
                </View>
              )}
            </ScrollView>
          </View>
        )}

        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    flex: 1,
  },
  dropDown: {
    height: hp('6'),
    width: wp('90%'),
    borderRadius: 6,
    borderWidth: 1,
    marginTop: 5,
    borderColor: colors.colorstiga,
    paddingLeft: 5,
  },
  textSaldo: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('3.5%'),
  },
  button: {
    backgroundColor: colors.colorsdua,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    width: wp('80%'),
    marginTop: hp('5%'),
    marginBottom: hp('5%'),
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('5%'),
    marginVertical: hp('2%'),
  },
  containerUserQurban: {
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  containerUserPayment: {
    // marginBottom: hp('2%'),
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  textUserQurban: {
    color: colors.colorBlack,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 12,
  },
  informasi: {
    width: wp('90%'),
    backgroundColor: '#FFD7001A',
    borderRadius: 5,
    borderColor: '#C2C2C226',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  informasitext: {
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 15,
    marginTop: hp('2%'),
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  buttoncancel: {
    backgroundColor: colors.colorsRed,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90'),
    marginBottom: hp('10'),
    borderRadius: 5,
  },
  textareaContainer: {
    height: hp('30%'),
    padding: 5,
    backgroundColor: '#F5FCFF',
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: wp('30%'),
    fontSize: 14,
    color: '#333',
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('5%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  imgcard: {
    marginTop: hp('5%'),
    width: wp('90%'),
    height: wp('90%'),
    borderWidth: 1,
  },
});

const mapStateToProps = ({
  AuthReducer,
  network,
  ContentReducer,
  TransaksiReducer,
}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListUploadPending);
