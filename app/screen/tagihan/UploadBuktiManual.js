import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Dropdown} from 'react-native-material-dropdown';
import {Header, Saldo, Loading, valueMoneyFormat} from './../../component';
import colors from '../../conf/color.global';
import {post_services, Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser} from './../../redux/actions';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import {api} from './../../conf/url';
import Select2 from 'react-native-select-two';
import Textarea from 'react-native-textarea';
import Icon from './../../component/icons';
import ImagePicker from 'react-native-image-picker';

class UploadBuktiManual extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tagihan_id: '',
      nomor_rekening: '',
      atas_nama: '',
      animating: false,
      warga: [],
      datawarga: [],
      imageUri: '',
      users_id: '',
      tagihan: [],
      datatagihan: [],
      bukti: [],
    };
  }

  OnSubmit() {
    this.setState({animating: true});

    const date = moment(new Date()).format('YYYY MMM DD HH:mm:ss');
    const m = this.state;
    const {navigation} = this.props;
    const data = new FormData();
    data.append('tagihan_id', m.tagihan_id[0]);
    data.append('users_id', m.users_id[0]);
    data.append('nomor_rekening', 'manual');
    data.append('foto', m.imageUri);
    data.append('atas_nama', 'manual');

    post_services(this.props.token, data, api.UploadBuktiPembayaranManual).then(
      rspons => {
        this.setState({animating: false});
        console.log(rspons);
        if (rspons.status == 200) {
          this.dropDownAlertRef.alertWithType(
            'success',
            'success',
            'Berhasil Upload Bukti Tagihan',
          );
          setTimeout(() => {
            this.props.navigation.navigate('Home');
          }, 7000);
        } else if (rspons.status == 401) {
          this.props.navigation.navigate('Login');
        } else {
          this.dropDownAlertRef.alertWithType(
            'error',
            'Error',
            rspons.data.message,
          );
        }
      },
    );
  }

  onActiveWarga() {
    Get_services(this.props.token, api.ActiveWarga).then(response => {
      if (response.status == 200) {
        this.setState({warga: response.data.data.warga});
        this.funGetwar();
        console.log(response.data.data.warga);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        this.setState({statusrt: true, mes: response.data.message});
        console.log(response, 'profile');
      }
    });
  }

  Onlisttagihan() {
    Get_services(this.props.token, api.AllTagihan).then(response => {
      if (response.status == 200) {
        this.setState({tagihan: response.data.data});
        this.funGetpro();
        console.log(this.state.tagihan);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  funGetpro() {
    this.state.tagihan.map((item, key) => {
      var joined = this.state.datatagihan.concat({
        id: item.id,
        name: item.jenis_tagihan,
      });
      this.setState({datatagihan: joined});
    });
  }
  funGetwar() {
    this.state.warga.map((item, key) => {
      var joined = this.state.datawarga.concat({
        id: item.users.id,
        name: item.users.name,
      });

      console.log(joined);
      this.setState({datawarga: joined});
    });
  }

  UNSAFE_componentWillMount() {
    this.Onlisttagihan();
    this.onActiveWarga();
  }

  onPressSelectImage() {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          name: response.fileName,
        };
        this.setState({
          imageUri: source,
        });
        // return this.detectText()
      }
    });
  }

  render() {
    const state = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading data={true} />
          </View>
        ) : (
          <View style={styles.container}>
            <Header
              onPress={() => navigation.goBack()}
              title="Upload Bukti Tagihan Manual"
            />
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nama Warga</Text>
                <Select2
                  isSelectSingle
                  style={{borderRadius: 5}}
                  colorTheme="blue"
                  popupTitle="Select item"
                  title="Select item"
                  data={this.state.datawarga}
                  onSelect={data => {
                    this.setState({users_id: data});
                  }}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Tagihan</Text>
                <Select2
                  isSelectSingle
                  style={{borderRadius: 5}}
                  colorTheme="blue"
                  popupTitle="Select item"
                  title="Select item"
                  data={this.state.datatagihan}
                  onSelect={data => {
                    this.setState({tagihan_id: data});
                  }}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.onPressSelectImage()}
                style={{
                  marginTop: hp('3'),
                  width: wp('90%'),
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: 'grey',
                }}>
                {state.imageUri == '' ? (
                  <>
                    <Icon.EvilIcons name="image" color="grey" size={wp('50')} />
                    <Text
                      style={{
                        fontFamily: 'ProximaNovaSemiBold',
                        color: 'grey',
                      }}>
                      {' '}
                      Upload Gambar
                    </Text>
                  </>
                ) : (
                  <Image
                    source={{uri: state.imageUri.uri}}
                    style={{
                      width: wp('90'),
                      height: hp('30'),
                      resizeMode: 'contain',
                    }}
                  />
                )}
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.OnSubmit()}
                style={styles.button}>
                <Text style={styles.textButton}>Submit</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        )}
        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    flex: 1,
  },
  dropDown: {
    height: hp('6'),
    width: wp('90%'),
    borderRadius: 6,
    borderWidth: 1,
    marginTop: 5,
    borderColor: colors.colorstiga,
    paddingLeft: 5,
  },
  textSaldo: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('3.5%'),
  },
  button: {
    backgroundColor: colors.colorsdua,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: hp('5%'),
    marginBottom: hp('5%'),
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('5%'),
    marginVertical: hp('2%'),
  },
  containerUserQurban: {
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  containerUserPayment: {
    // marginBottom: hp('2%'),
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  textUserQurban: {
    color: colors.colorBlack,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 12,
  },
  informasi: {
    width: wp('90%'),
    backgroundColor: '#FFD7001A',
    borderRadius: 5,
    borderColor: '#C2C2C226',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  informasitext: {
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 15,
    marginTop: hp('2%'),
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  buttoncancel: {
    backgroundColor: colors.colorsRed,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90'),
    marginBottom: hp('10'),
    borderRadius: 5,
  },
  textareaContainer: {
    height: hp('30%'),
    padding: 5,
    backgroundColor: '#F5FCFF',
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: wp('30%'),
    fontSize: 14,
    color: '#333',
  },
});

const mapStateToProps = ({
  AuthReducer,
  network,
  ContentReducer,
  TransaksiReducer,
}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UploadBuktiManual);
