import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Header, Loading} from './../../component';
import colors from '../../conf/color.global';
import {Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser} from './../../redux/actions';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import {api} from './../../conf/url';
import Select2 from 'react-native-select-two';
import Icon from './../../component/icons';
import LottieView from 'lottie-react-native';

class ListUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      warga: [],
      datawarga: [],
      tagihan: [],
      datatagihan: [],
      bukti: [],
      waga_id: '',
    };
  }

  OnSubmit() {
    this.setState({animating: true});

    const m = this.state;
    var tagihan = m.tagihan_id[0];
    var war = m.waga_id[0];
    console.log(war);
    if (war == null) {
      var warga = this.props.infoUser.id;
    } else {
      var warga = war;
    }
    Get_services(
      this.props.token,
      api.TagihanUser + warga + '/' + tagihan,
    ).then(rspons => {
      this.setState({animating: false});
      console.log(rspons);
      if (rspons.status == 200) {
        this.setState({bukti: rspons.data.data});
      } else if (rspons.status == 401) {
        this.props.navigation.navigate('Login');
      } else {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          rspons.data.message,
        );
      }
    });
  }

  onActiveWarga() {
    Get_services(this.props.token, api.ActiveWarga).then(response => {
      if (response.status == 200) {
        this.setState({warga: response.data.data.warga});
        this.funGetwar();
        console.log(response.data.data.warga);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        this.setState({statusrt: true, mes: response.data.message});
        console.log(response, 'profile');
      }
    });
  }

  Onlisttagihan() {
    Get_services(this.props.token, api.AllTagihan).then(response => {
      if (response.status == 200) {
        this.setState({tagihan: response.data.data});
        this.funGetpro();
        console.log(this.state.tagihan);
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  funGetpro() {
    this.state.tagihan.map((item, key) => {
      var joined = this.state.datatagihan.concat({
        id: item.id,
        name: item.jenis_tagihan,
      });
      this.setState({datatagihan: joined});
    });
  }
  funGetwar() {
    this.state.warga.map((item, key) => {
      var joined = this.state.datawarga.concat({
        id: item.users.id,
        name: item.users.name,
      });

      console.log(joined);
      this.setState({datawarga: joined});
    });
  }

  UNSAFE_componentWillMount() {
    this.Onlisttagihan();
    this.onActiveWarga();
  }

  render() {
    const state = this.state;
    const {navigation} = this.props;
    const List = ({onPress, title, email, hp, tgl}) => {
      return (
        <TouchableOpacity style={styles.containerAddress} onPress={onPress}>
          <View style={styles.subContainerAddress}>
            <Text>Atas Nama</Text>
            <Text style={styles.textList}>{title}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>No. Rekening</Text>
            <Text style={styles.textList}>{email}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Status</Text>
            <Text style={styles.textList}>{hp}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Tanggal</Text>
            <Text style={styles.textList}>{tgl}</Text>
          </View>
        </TouchableOpacity>
      );
    };
    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading data={true} />
          </View>
        ) : (
          <View style={styles.container}>
            <Header
              onPress={() => navigation.goBack()}
              title="List Bukti Tagihan "
            />
            {this.props.infoUser.profile == null ? (
              ''
            ) : this.props.infoUser.profile.role == 'RT' ? (
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nama Warga</Text>
                <Select2
                  isSelectSingle
                  style={{borderRadius: 5}}
                  colorTheme="blue"
                  popupTitle="Select item"
                  title="Select item"
                  data={this.state.datawarga}
                  onSelect={data => {
                    this.setState({waga_id: data});
                  }}
                />
              </View>
            ) : null}
            <View style={styles.containerUserPayment}>
              <Text style={styles.textSaldo}>Tagihan</Text>
              <Select2
                isSelectSingle
                style={{borderRadius: 5}}
                colorTheme="blue"
                popupTitle="Select item"
                title="Select item"
                data={this.state.datatagihan}
                onSelect={data => {
                  this.setState({tagihan_id: data});
                }}
              />
            </View>
            <TouchableOpacity
              onPress={() => this.OnSubmit()}
              style={styles.button}>
              <Text style={styles.textButton}>Submit</Text>
            </TouchableOpacity>
            <ScrollView showsVerticalScrollIndicator={false}>
              {this.state.bukti.length != 0 ? (
                <>
                  {this.state.bukti.map((item, key) => {
                    return (
                      <>
                        <List
                          title={item.atas_nama}
                          email={item.nomor_rekening}
                          hp={item.status}
                          tgl={moment(item.created_at).format('D MMMM YYYY')}
                        />
                      </>
                    );
                  })}
                </>
              ) : (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      backgroundColor: 'transparent',
                      borderRadius: 10,
                      width: wp('100'),
                      height: wp('100'),
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <LottieView
                      source={require('../../assets/lottie/empaty.json')}
                      autoPlay
                      loop
                    />
                  </View>
                </View>
              )}
            </ScrollView>
          </View>
        )}

        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    flex: 1,
  },
  dropDown: {
    height: hp('6'),
    width: wp('90%'),
    borderRadius: 6,
    borderWidth: 1,
    marginTop: 5,
    borderColor: colors.colorstiga,
    paddingLeft: 5,
  },
  textSaldo: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('3.5%'),
  },
  button: {
    backgroundColor: colors.colorsdua,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    width: wp('90%'),
    marginTop: hp('5%'),
    marginBottom: hp('5%'),
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('5%'),
    marginVertical: hp('2%'),
  },
  containerUserQurban: {
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  containerUserPayment: {
    // marginBottom: hp('2%'),
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  textUserQurban: {
    color: colors.colorBlack,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 12,
  },
  informasi: {
    width: wp('90%'),
    backgroundColor: '#FFD7001A',
    borderRadius: 5,
    borderColor: '#C2C2C226',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  informasitext: {
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 15,
    marginTop: hp('2%'),
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  buttoncancel: {
    backgroundColor: colors.colorsRed,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90'),
    marginBottom: hp('10'),
    borderRadius: 5,
  },
  textareaContainer: {
    height: hp('30%'),
    padding: 5,
    backgroundColor: '#F5FCFF',
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: wp('30%'),
    fontSize: 14,
    color: '#333',
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('5%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
});

const mapStateToProps = ({
  AuthReducer,
  network,
  ContentReducer,
  TransaksiReducer,
}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListUpload);
