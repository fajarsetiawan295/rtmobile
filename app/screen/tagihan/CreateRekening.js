import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Dropdown} from 'react-native-material-dropdown';
import {Header, Saldo, Loading, valueMoneyFormat} from './../../component';
import colors from '../../conf/color.global';
import {post_services, Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser} from './../../redux/actions';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import {api} from './../../conf/url';
import Select2 from 'react-native-select-two';

class CreateNomorRekening extends Component {
  constructor(props) {
    super(props);
    this.state = {
      atas_nama: '',
      nomor_rekening: '',
      nama_bank: '',
      animating: false,
    };
  }

  OnSubmit() {
    this.setState({animating: true});

    const date = moment(new Date()).format('YYYY MMM DD HH:mm:ss');
    const m = this.state;
    const {navigation} = this.props;
    const data = new FormData();
    data.append('atas_nama', m.atas_nama);
    data.append('nomor_rekening', m.nomor_rekening);
    data.append('nama_bank', m.nama_bank);

    post_services(this.props.token, data, api.CreateNomorRekening).then(
      rspons => {
        this.setState({animating: false});
        console.log(rspons);
        if (rspons.status == 200) {
          this.dropDownAlertRef.alertWithType(
            'success',
            'success',
            'Berhasil Menambahkan Rekening',
          );
          setTimeout(() => {
            this.props.navigation.navigate('Home');
          }, 7000);
        } else if (rspons.status == 401) {
          this.props.navigation.navigate('Login');
        } else {
          this.dropDownAlertRef.alertWithType(
            'error',
            'Error',
            rspons.data.message,
          );
        }
      },
    );
  }

  render() {
    const state = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading data={true} />
          </View>
        ) : (
          <View style={styles.container}>
            <Header
              onPress={() => navigation.goBack()}
              title="Tambah Rekening"
            />
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nama Pemilik Rekening</Text>
                <TextInput
                  value={state.atas_nama}
                  placeholder={'Nama Pemilik Rekening'}
                  onChangeText={x => this.setState({atas_nama: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nomor Rekening</Text>
                <TextInput
                  value={state.nomor_rekening}
                  placeholder={'Nomor Rekening'}
                  keyboardType={'numeric'}
                  onChangeText={x => this.setState({nomor_rekening: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nama BANK Penerima</Text>
                <TextInput
                  value={state.nama_bank}
                  placeholder={'Nama BANK Penerima'}
                  onChangeText={x => this.setState({nama_bank: x})}
                  style={styles.dropDown}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.OnSubmit()}
                style={styles.button}>
                <Text style={styles.textButton}>Submit</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        )}
        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    flex: 1,
  },
  dropDown: {
    height: hp('6'),
    width: wp('90%'),
    borderRadius: 6,
    borderWidth: 1,
    marginTop: 5,
    borderColor: colors.colorstiga,
    paddingLeft: 5,
  },
  textSaldo: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('3.5%'),
  },
  button: {
    backgroundColor: colors.colorsdua,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: hp('5%'),
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('5%'),
    marginVertical: hp('2%'),
  },
  containerUserQurban: {
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  containerUserPayment: {
    // marginBottom: hp('2%'),
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  textUserQurban: {
    color: colors.colorBlack,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 12,
  },
  informasi: {
    width: wp('90%'),
    backgroundColor: '#FFD7001A',
    borderRadius: 5,
    borderColor: '#C2C2C226',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  informasitext: {
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 15,
    marginTop: hp('2%'),
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  buttoncancel: {
    backgroundColor: colors.colorsRed,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90'),
    marginBottom: hp('10'),
    borderRadius: 5,
  },
});

const mapStateToProps = ({
  AuthReducer,
  network,
  ContentReducer,
  TransaksiReducer,
}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateNomorRekening);
