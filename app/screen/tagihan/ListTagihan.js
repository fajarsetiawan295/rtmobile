import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Icon, Header} from './../../component';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged, tokenUser} from './../../redux/actions';
import {api} from './../../conf/url';
import {Get_services} from './../../services';
import LottieView from 'lottie-react-native';

class ActiveWarga extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nomor: [],
      statusrt: false,
      mes: '',
    };
  }
  onLIstRekening() {
    Get_services(this.props.token, api.AllTagihan).then(response => {
      if (response.status == 200) {
        console.log(response);
        this.setState({nomor: response.data.data});
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        this.setState({statusrt: true, mes: response.data.message});
        console.log(response, 'profile');
      }
    });
  }

  UNSAFE_componentWillMount() {
    this.onLIstRekening();
  }
  render() {
    const {navigation} = this.props;
    const List = ({onPress, title, email, tgl, rek, nm, ats}) => {
      return (
        <TouchableOpacity style={styles.containerAddress} onPress={onPress}>
          <View style={styles.subContainerAddress}>
            <Text>Jenis Tagihan</Text>
            <Text style={styles.titikdua}>:</Text>

            <Text style={styles.textList}>{title}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Nominal</Text>
            <Text style={styles.titikdua}>:</Text>
            <Text style={styles.textList}>Rp. {email}</Text>
          </View>
          <View style={styles.subContainerAddress}>
            <Text>Tagihan</Text>
            <Text style={styles.titikdua}>:</Text>
            <Text style={styles.textList}>Tanggal {tgl}</Text>
          </View>
          <View style={styles.nomorrekening}>
            <View style={styles.subnomorrekening}>
              <Text>Nomor Rekening</Text>
              <Text style={styles.titikdua}>:</Text>
              <Text style={styles.textList}>{rek}</Text>
            </View>
            <View style={styles.subnomorrekening}>
              <Text>Nama Pengirim</Text>
              <Text style={styles.titikdua}>:</Text>
              <Text style={styles.textList}>{ats}</Text>
            </View>
            <View style={styles.subnomorrekening}>
              <Text>Nama Bank</Text>
              <Text style={styles.titikdua}>:</Text>
              <Text style={styles.textList}>{nm}</Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.container}>
        <Header onPress={() => navigation.goBack()} title={'List Tagihan'} />
        {this.props.infoUser.profile == null ? (
          ''
        ) : this.props.infoUser.profile.role == 'RT' ? (
          <View style={styles.containerAddAddres}>
            <TouchableOpacity
              style={styles.buttonAddAddress}
              onPress={() => this.props.navigation.navigate('CreateTagihan')}>
              <Text style={styles.textAddAddress}>Tambah Tagihan</Text>
              <Icon.AntDesign
                name="pluscircleo"
                color={colors.colorsdua}
                size={24}
              />
            </TouchableOpacity>
          </View>
        ) : null}

        {this.state.nomor.length != '0' ? (
          <>
            {this.state.nomor.map((item, key) => {
              return (
                <>
                  <List
                    title={item.jenis_tagihan}
                    email={item.nominal}
                    tgl={item.tanggal}
                    rek={item.rekening.nomor_rekening}
                    ats={item.rekening.atas_nama}
                    nm={item.rekening.nama_bank}
                  />
                </>
              );
            })}
          </>
        ) : (
          <>
            <View
              style={{
                backgroundColor: 'transparent',
                borderRadius: 10,
                width: wp('100'),
                height: wp('100'),
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <LottieView
                source={require('../../assets/lottie/empaty.json')}
                autoPlay
                loop
              />
            </View>
          </>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#E9EBE4',
  },
  containerProfile: {
    marginTop: hp('5'),
    width: wp('90'),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileRight: {
    width: wp('55'),
  },
  imageProfile: {
    width: wp('25'),
    height: wp('25'),
    borderRadius: wp('20'),
    borderWidth: 1,
    borderColor: 'white',
  },
  textNameProfile: {
    // fontFamily: Fonts.type.emphasis,
    // fontSize: Fonts.size.regular,
  },
  textEmailProfile: {
    // fontSize: Fonts.size.small,
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  buttonEditProfile: {
    marginTop: hp('2'),
    borderRadius: 14,
    width: wp('50'),
    borderColor: '#727C8E4D',
    borderWidth: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButtonEditProfile: {
    // fontFamily: Fonts.type.bold,
    // fontSize: Fonts.size.medium,
    color: '#727C8E',
    marginVertical: wp('1'),
  },
  containerAddress: {
    marginVertical: hp('1'),
    width: wp('90'),
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: wp('5%'),
  },
  subContainerAddress: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('80'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  textList: {
    width: wp('50'),
    // fontFamily: Fonts.type.emphasis,
    color: '#515C6F',
  },
  containerAddAddres: {
    marginTop: hp('2'),
    alignItems: 'flex-end',
    width: wp('90'),
  },
  buttonAddAddress: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 1,
    width: wp('50%'),
    height: hp('5%'),
    borderRadius: wp('5%'),
    borderColor: colors.colorsdua,
  },
  textAddAddress: {
    marginRight: wp('2'),
    color: colors.colorsdua,
  },

  titikdua: {marginLeft: wp('1%'), marginRight: wp('1%')},
  nomorrekening: {
    marginVertical: hp('1'),
    borderWidth: 1,
    width: wp('70'),
    height: hp('15'),
    alignItems: 'center',
  },
  subnomorrekening: {
    marginVertical: hp('1'),
    justifyContent: 'space-between',
    width: wp('65'),
    alignItems: 'center',
    flexDirection: 'row',
  },
});

const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {isConnected, token, infoUser};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({dataUser, isLogged, tokenUser}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActiveWarga);
