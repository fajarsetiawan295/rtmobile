import React, {Component} from 'react';
import {
  Alert,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Image,
  CheckBox,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from './../../conf';
import {post_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isLogged, tokenUser} from '../../redux/actions';
import DropdownAlert from 'react-native-dropdownalert';
import {api} from './../../conf/url';
import {Loading} from './../../component';
import Modal from 'react-native-modal';

class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      passwd: '',
      tok: '',
      lifetime: false,
      isLoading: false,
      animating: false,
      isModalVisible: false,
    };
  }

  onLoginPress() {
    this.setState({animating: true});
    const state = this.state;
    const {navigation} = this.props;

    const data = new FormData();
    data.append('email', state.email);
    data.append('password', state.passwd);

    post_services(null, data, api.Login).then(rspons => {
      this.setState({animating: false});
      console.log(rspons);
      if (rspons.status == 200) {
        if (rspons.data.status == true) {
          this.FCMTOKEN(rspons.data.access_token);
          this.props.tokenUser(rspons.data.access_token);
          this.setState({tok: rspons.data.access_token});
          this.inputkeydua();
          this.props.isLogged(true);
          navigation.navigate('Home');
        } else if (rspons.data.status == false) {
          this.setState({tok: rspons.data.access_token});
          this.setState({isModalVisible: true});
        }
      } else {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          rspons.data.message,
        );
      }
    });
  }

  inputkey() {
    this.setState({animating: true});
    const state = this.state;
    const {navigation} = this.props;

    const data = new FormData();
    data.append('keyRT', state.otp);

    post_services(this.state.tok, data, api.InputKey).then(rspons => {
      this.setState({animating: false});
      console.log(rspons, 'ini key');
      if (rspons.status == 200) {
        this.onLoginPress();
      } else {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          rspons.data.message,
        );
      }
    });
  }
  inputkeydua() {
    const state = this.state;
    const {navigation} = this.props;
    const data = new FormData();
    data.append('keyRT', state.otp);
    post_services(this.state.tok, data, api.InputKey).then(rspons => {
      this.setState({animating: false});
      console.log(rspons, 'ini key');
    });
  }

  FCMTOKEN(x) {
    this.setState({animating: true});
    const state = this.state;
    const {navigation} = this.props;

    const data = new FormData();
    data.append('email', state.email);
    data.append('token', this.props.tokenfcm);

    post_services(x, data, api.FCMTOKEN).then(rspons => {
      this.setState({animating: false});
      console.log(rspons);
      if (rspons.status == 200) {
        console.log('Berhasil');
      } else {
        console.log('gagal');
      }
    });
  }

  onRenderFilterDate() {
    const state = this.state;
    return (
      <Modal isVisible={this.state.isModalVisible}>
        <View
          style={{
            backgroundColor: '#ffffff',
            borderRadius: 6,
            alignItems: 'center',
            height: hp('25%'),
          }}>
          <View style={styles.cardFormInputmodal}>
            <Text style={styles.textFormTitle}>Key RT</Text>
            <TextInput
              value={state.otp}
              style={styles.formText}
              keyboardType={'numeric'}
              onChangeText={x => this.setState({otp: x})}
            />
          </View>
          <TouchableOpacity
            onPress={() => this.inputkey()}
            style={styles.button}>
            {state.isLoading == true ? (
              <ActivityIndicator color={'red'} size="small" />
            ) : (
              <Text style={styles.textButton}>Submit</Text>
            )}
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  render() {
    const {navigation} = this.props;
    const state = this.state;
    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading />
          </View>
        ) : (
          <View style={styles.container}>
            <ScrollView
              contentContainerStyle={{alignItems: 'center'}}
              showsVerticalScrollIndicator={false}>
              {this.onRenderFilterDate()}
              <View style={styles.cardFormInputemail}>
                <Text style={styles.textFormTitle}>Email</Text>
                <TextInput
                  value={state.email}
                  style={styles.formText}
                  onChangeText={x => this.setState({email: x})}
                />
              </View>
              <View style={styles.cardFormInput}>
                <Text
                  style={{
                    color: colors.colorsdua,
                    fontFamily: 'ProximaNova',
                    fontSize: wp('4.5%'),
                  }}>
                  Password
                </Text>
                <TextInput
                  value={state.passwd}
                  style={styles.formText}
                  secureTextEntry={true}
                  onChangeText={x => this.setState({passwd: x})}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.onLoginPress()}
                style={styles.button}>
                {state.isLoading == true ? (
                  <ActivityIndicator color={'red'} size="small" />
                ) : (
                  <Text style={styles.textButton}>LOG IN</Text>
                )}
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => navigation.navigate('Register')}
                style={styles.buttonregister}>
                <Text style={{color: colors.colorsdua}}>Register</Text>
              </TouchableOpacity>

              {/* <TouchableOpacity style={{marginTop: hp('4%')}}>
                <Text style={styles.textQuestionAccount}>
                  <Text style={{color: colors.colorsdua}}>Forgot Password</Text>
                </Text>
              </TouchableOpacity> */}
            </ScrollView>
          </View>
        )}
        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textFormTitle: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNova',
    fontSize: wp('4.5%'),
  },
  cardFormInput: {
    width: wp('80%'),
    marginTop: hp('2%'),
  },
  cardFormInputemail: {
    width: wp('80%'),
    marginTop: hp('30%'),
  },
  formText: {
    height: hp('6'),
    width: wp('80%'),
    paddingLeft: 10,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: colors.colorstiga,
    color: colors.colorsdua,
    fontFamily: 'ProximaNova',
    // elevation: 1,
    // opacity: 0.99
  },
  button: {
    width: wp('80%'),
    marginTop: hp('4%'),
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorstiga,
    height: hp('6%'),
  },
  buttonregister: {
    width: wp('80%'),
    marginTop: hp('4%'),
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.colorstiga,
    height: hp('6%'),
  },
  checkboxContent: {
    flexDirection: 'row',
    width: wp('80'),
    alignItems: 'center',
    marginTop: hp('1'),
  },
  textButton: {
    fontFamily: 'ProximaNova',
    color: colors.colorsdua,
    fontSize: wp('5%'),
  },
  textQuestionAccount: {
    fontFamily: 'ProximaNova',
    color: '#626270',
    fontSize: wp('4.5%'),
  },
  imageLogo: {
    width: wp('70%'),
    height: hp('20%'),
    resizeMode: 'cover',
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({tokenUser, isLogged}, dispatch);
};
const mapStateToProps = ({AuthReducer}) => {
  const {tokenfcm} = AuthReducer;
  return {tokenfcm};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(login);
