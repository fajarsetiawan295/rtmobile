import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  YellowBox,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {FlatGrid} from 'react-native-super-grid';
import Icon from './../../component/icons';
import {Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged} from './../../redux/actions';
import {api, url} from './../../conf/url';
import {Loading, Header} from './../../component';
import icons from './../../component/icons';
import LottieView from 'lottie-react-native';
import moment from 'moment';

YellowBox.ignoreWarnings([
  'Calling `getNode()` on the ref of an Animated component is no longer necessary. You can now directly use the ref instead.',
  'VirtualizedLists should never be nested',
]);

class DetailsPengaduan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      informasi: {},
      userpenfadu: {},
      datainformasi: [],
      refresh: false,
    };
  }

  onGetInformasi() {
    const {navigation} = this.props;
    Get_services(
      this.props.token,
      api.DetailsPengaduan + navigation.getParam('id'),
    ).then(response => {
      console.log('ini', response);
      if (response.status == 200) {
        this.setState({informasi: response.data.data});
        this.setState({userpenfadu: response.data.data.user});
        this.setState({datainformasi: response.data.data.status_pengaduan});
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  UNSAFE_componentWillMount() {
    this.onRequestAll();
  }

  onRequestAll() {
    this.onGetInformasi();
  }
  refreshContent() {
    this.setState({refresh: true});
    this.onRequestAll();
    this.setState({refresh: false});
  }

  render() {
    const {navigation} = this.props;
    const img = require('./../../assets/image/test.png');
    const List = ({foto, deskripsi, status, tgl}) => {
      return (
        <View style={styles.containercard}>
          <View style={styles.subcontainercard}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <TouchableOpacity style={styles.card}>
                <Image
                  source={{
                    uri: url + foto,
                  }}
                  style={styles.imgcard}
                />
                <Text style={styles.judulcard}>{deskripsi}</Text>
                <View
                  style={{
                    width: wp('85%'),
                    alignItems: 'flex-end',
                    marginBottom: hp('2%'),
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  <Text>{tgl}</Text>
                  <Text>{status}</Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      );
    };
    return (
      // header
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading />
          </View>
        ) : (
          <View style={styles.container}>
            <Header
              onPress={() => navigation.goBack()}
              title="Details Pengaduan"
            />
            <View style={styles.containerAddAddres}>
              <TouchableOpacity
                style={styles.buttonAddAddress}
                onPress={() =>
                  this.props.navigation.navigate('BalasanPengaduan', {
                    id: this.state.informasi.id,
                  })
                }>
                <Text style={styles.textAddAddress}>Balas Pengaduan</Text>
                <Icon.AntDesign
                  name="pluscircleo"
                  color={colors.colorsdua}
                  size={24}
                />
              </TouchableOpacity>
            </View>
            {this.state.informasi.length != 0 ? (
              <ScrollView
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refresh}
                    onRefresh={() => this.refreshContent()}
                  />
                }
                showsVerticalScrollIndicator={false}>
                <View style={styles.containercard}>
                  <View style={styles.subcontainercard}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                      <TouchableOpacity style={styles.card}>
                        <Image
                          source={{
                            uri: url + this.state.informasi.foto,
                          }}
                          style={styles.imgcard}
                        />
                        <Text style={styles.judulcard}>
                          {this.state.informasi.deskripsi}
                        </Text>
                        <Text style={styles.judulcard}>
                          {this.state.userpenfadu.name}
                        </Text>
                        <View
                          style={{
                            width: wp('85%'),
                            alignItems: 'flex-end',
                            marginBottom: hp('2%'),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <Text style={styles.judulcard}>
                            {moment(this.state.informasi.created_at).format(
                              'L',
                            )}
                          </Text>
                          <Text style={styles.judulcard}>
                            {this.state.informasi.status}
                          </Text>
                        </View>
                      </TouchableOpacity>

                      <Text style={{fontWeight: 'bold'}}>Respon RT</Text>
                      <ScrollView
                        refreshControl={
                          <RefreshControl
                            refreshing={this.state.refresh}
                            onRefresh={() => this.refreshContent()}
                          />
                        }
                        showsVerticalScrollIndicator={false}>
                        {this.state.datainformasi.map((item, key) => {
                          return (
                            <>
                              <List
                                foto={item.foto}
                                deskripsi={item.deskripsi}
                                status={item.status}
                                tgl={moment(item.created_at).format('L')}
                              />
                            </>
                          );
                        })}
                      </ScrollView>
                    </ScrollView>
                  </View>
                </View>
              </ScrollView>
            ) : (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text>{this.state.mes}</Text>
                <View
                  style={{
                    backgroundColor: 'transparent',
                    borderRadius: 10,
                    width: wp('100'),
                    height: wp('100'),
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <LottieView
                    source={require('../../assets/lottie/empaty.json')}
                    autoPlay
                    loop
                  />
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.YellowButton,
    alignItems: 'center',
  },
  imageProfile: {
    marginTop: hp('1%'),
    marginLeft: wp('2%'),
    width: wp('20'),
    height: wp('20'),
    borderRadius: wp('20'),
    borderWidth: 1,
  },
  containerIconGrid: {
    width: wp('95%'),
    backgroundColor: colors.colorstiga,
    borderRadius: 10,
    borderTopLeftRadius: wp('10%'),
    borderTopRightRadius: wp('10%'),
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  subContainerIconGrid: {
    margin: wp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp('90%'),
  },
  componentGridIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('20%'),
  },
  subComponentGridCyrcle: {
    borderRadius: wp('20%'),
    backgroundColor: 'white',
    height: wp('15%'),
    width: wp('15%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  subComponentGridText: {
    marginTop: 1,
    fontFamily: 'ProximaNova',
    fontSize: wp('3%'),
    color: colors.colorssatu,
    width: wp('22%'),
    alignItems: 'center',
    textAlign: 'center',
  },
  containercard: {
    width: wp('90%'),
    marginTop: hp('1%'),
  },
  textcategory: {
    fontSize: wp('5%'),
    fontWeight: 'bold',
  },
  subcontainercard: {
    width: wp('90%'),
  },
  card: {
    marginTop: hp('2%'),
    width: wp('90%'),
    // height: hp('35%'),
    marginBottom: wp('2%'),
    borderRadius: wp('3%'),
    borderColor: colors.colorstiga,
    borderWidth: 1,
    backgroundColor: 'white',
  },
  imgcard: {
    borderRadius: wp('3%'),

    width: wp('90%'),
    height: wp('70%'),
    borderWidth: 1,
  },
  judulcard: {
    marginLeft: wp('2%'),
    marginRight: wp('3%'),
    marginTop: hp('1%'),
    fontWeight: 'bold',
    // marginBottom: hp('2%'),
  },
  iconcomment: {
    marginTop: hp('3%'),
    width: wp('40%'),
    alignItems: 'flex-end',
  },
  containerAddAddres: {
    marginTop: hp('2'),
    alignItems: 'flex-end',
    width: wp('90'),
  },
  buttonAddAddress: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 1,
    width: wp('50%'),
    height: hp('5%'),
    borderRadius: wp('5%'),
    borderColor: colors.colorsdua,
  },
  textAddAddress: {
    marginRight: wp('2'),
    color: colors.colorsdua,
  },
});

// export default home;
const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
      isLogged,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailsPengaduan);
