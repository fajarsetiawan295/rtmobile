import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Dropdown} from 'react-native-material-dropdown';
import {Header, Saldo, Loading, valueMoneyFormat} from './../../component';
import colors from '../../conf/color.global';
import {post_services, Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser} from './../../redux/actions';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import {api} from './../../conf/url';
import Select2 from 'react-native-select-two';

class IsiSurat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rt: '',
      rw: '',
      kel: '',
      kec: '',
      kab: '',
      nama_rw: '',
      nama_rt: '',
      nomor: [],
      datano: [],
      animating: false,
    };
  }

  OnSubmit() {
    this.setState({animating: true});

    const date = moment(new Date()).format('YYYY MMM DD HH:mm:ss');
    const m = this.state;
    const {navigation} = this.props;
    const data = new FormData();
    data.append('rt', m.rt);
    data.append('rw', m.rw);
    data.append('kel', m.kel);
    data.append('kab', m.kab);
    data.append('kec', m.kec);
    data.append('nama_rt', m.nama_rt);
    data.append('nama_rw', m.nama_rw);

    post_services(this.props.token, data, api.CreateBodySurat).then(rspons => {
      this.setState({animating: false});
      console.log(rspons);
      if (rspons.status == 200) {
        this.dropDownAlertRef.alertWithType(
          'success',
          'success',
          'Berhasil Menambahkan Isi Surat',
        );
        setTimeout(() => {
          this.props.navigation.navigate('Home');
        }, 7000);
      } else if (rspons.status == 401) {
        this.props.navigation.navigate('Login');
      } else {
        this.dropDownAlertRef.alertWithType(
          'error',
          'Error',
          rspons.data.message,
        );
      }
    });
  }
  onLIstRekening() {
    Get_services(this.props.token, api.AllNomorRekening).then(response => {
      if (response.status == 200) {
        this.setState({nomor: response.data.data});
        this.funGetpro();
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        this.setState({statusrt: true, mes: response.data.message});
        console.log(response, 'profile');
      }
    });
  }

  UNSAFE_componentWillMount() {
    this.onLIstRekening();
  }

  funGetpro() {
    this.state.nomor.map((item, key) => {
      var joined = this.state.datano.concat({
        id: item.id,
        name: item.atas_nama,
      });
      this.setState({datano: joined});
    });
  }

  render() {
    const state = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading data={true} />
          </View>
        ) : (
          <View style={styles.container}>
            <Header
              onPress={() => navigation.goBack()}
              title="Isi Body Surat"
            />
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>RT</Text>
                <TextInput
                  value={state.rt}
                  placeholder={'RT'}
                  keyboardType={'numeric'}
                  onChangeText={x => this.setState({rt: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>RW</Text>
                <TextInput
                  value={state.rw}
                  placeholder={'RW'}
                  keyboardType={'numeric'}
                  onChangeText={x => this.setState({rw: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Kelurahan</Text>
                <TextInput
                  value={state.kel}
                  placeholder={'Kelurahan'}
                  onChangeText={x => this.setState({kel: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Kecamatan</Text>
                <TextInput
                  value={state.kec}
                  placeholder={'Kecamatan'}
                  onChangeText={x => this.setState({kec: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Kabupaten / Kota</Text>
                <TextInput
                  value={state.kab}
                  placeholder={'Kabupaten / Kota'}
                  onChangeText={x => this.setState({kab: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nama RT</Text>
                <TextInput
                  value={state.nama_rt}
                  placeholder={'Nama RT'}
                  onChangeText={x => this.setState({nama_rt: x})}
                  style={styles.dropDown}
                />
              </View>
              <View style={styles.containerUserPayment}>
                <Text style={styles.textSaldo}>Nama RW</Text>
                <TextInput
                  value={state.nama_rw}
                  placeholder={'Nama RW'}
                  onChangeText={x => this.setState({nama_rw: x})}
                  style={styles.dropDown}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.OnSubmit()}
                style={styles.button}>
                <Text style={styles.textButton}>Submit</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        )}
        <DropdownAlert
          ref={ref => (this.dropDownAlertRef = ref)}
          closeInterval={5000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    flex: 1,
  },
  dropDown: {
    height: hp('6'),
    width: wp('90%'),
    borderRadius: 6,
    borderWidth: 1,
    marginTop: 5,
    borderColor: colors.colorstiga,
    paddingLeft: 5,
  },
  textSaldo: {
    color: colors.colorsdua,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('3.5%'),
  },
  button: {
    backgroundColor: colors.colorsdua,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginTop: hp('5%'),
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: wp('5%'),
    marginVertical: hp('2%'),
  },
  containerUserQurban: {
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  containerUserPayment: {
    // marginBottom: hp('2%'),
    width: wp('90%'),
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  textUserQurban: {
    color: colors.colorBlack,
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 12,
  },
  informasi: {
    width: wp('90%'),
    backgroundColor: '#FFD7001A',
    borderRadius: 5,
    borderColor: '#C2C2C226',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  informasitext: {
    fontFamily: 'ProximaNovaSemiBold',
    fontSize: 15,
    marginTop: hp('2%'),
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  buttoncancel: {
    backgroundColor: colors.colorsRed,
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('90'),
    marginBottom: hp('10'),
    borderRadius: 5,
  },
});

const mapStateToProps = ({
  AuthReducer,
  network,
  ContentReducer,
  TransaksiReducer,
}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IsiSurat);
