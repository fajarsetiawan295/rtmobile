export {default as CreateInformasi} from './CreateInformasi';
export {default as CommentInformasi} from './Comment';
export {default as BalasanCommentar} from './BalasanCommentar';
export {default as ListUpload} from './ListUpload';
export {default as UploadBuktiInformasi} from './UploadBuktiInformasi';
