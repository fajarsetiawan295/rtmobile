import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isLogged, tokenfcm} from '../redux/actions';
import messaging from '@react-native-firebase/messaging';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class splashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  UNSAFE_componentWillMount() {
    this.onGetToken();
    const {navigation} = this.props;
    if (this.props.isLogged) {
      setTimeout(() => {
        this.props.navigation.navigate('Home');
      }, 3500);
    } else {
      setTimeout(() => {
        navigation.navigate('Login');
      }, 3500);
    }
  }

  onGetToken() {
    messaging()
      .getToken()
      .then(token => {
        this.props.tokenfcm(token);
        console.log(token, 'token');
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            width: wp('90%'),
            height: hp('100%'),
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{width: wp('90%'), height: hp('90%')}}
            source={require('./../assets/image/neighbourhood-removebg-preview.png')}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  imageSplash: {
    width: wp('80'),
    height: hp('60'),
  },
});

const mapStateToProps = ({AuthReducer}) => {
  const {isLogged} = AuthReducer;
  return {isLogged};
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({tokenfcm}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(splashScreen);
