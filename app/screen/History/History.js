import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  YellowBox,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {colors} from '../../conf';
import {FlatGrid} from 'react-native-super-grid';
import Icon from './../../component/icons';
import {Get_services} from './../../services';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {dataUser, isLogged} from './../../redux/actions';
import {api, url} from './../../conf/url';
import {Loading} from './../../component';
import icons from './../../component/icons';

YellowBox.ignoreWarnings([
  'Calling `getNode()` on the ref of an Animated component is no longer necessary. You can now directly use the ref instead.',
  'VirtualizedLists should never be nested',
]);

class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headItem: [],
      headItemdua: [],
      animating: false,
      informasi: [],
      refresh: false,
    };
  }

  onGetProfile() {
    this.setState({animating: true});
    Get_services(this.props.token, api.Profile).then(response => {
      if (response.status == 200) {
        this.props.dataUser(response.data.data);
        this.funIcon();
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }
  onGetInformasi() {
    Get_services(this.props.token, api.AllCategory).then(response => {
      console.log('ini', response);
      if (response.status == 200) {
        this.setState({informasi: response.data.data});
      } else if (response.status == 401) {
        this.props.isLogged(false);
        this.props.navigation.navigate('Login');
      } else {
        console.log(response, 'profile');
      }
    });
  }

  UNSAFE_componentWillMount() {
    this.onRequestAll();
  }

  onRequestAll() {
    this.onGetProfile();
    this.onGetInformasi();
  }
  refreshContent() {
    this.setState({refresh: true});
    this.onRequestAll();
    this.setState({refresh: false});
  }

  funIcon() {
    if (
      this.props.infoUser.profile == null
        ? ''
        : this.props.infoUser.profile.role == 'RT'
    ) {
      this.setState({
        headItem: [
          {
            item: 'Rekening',
            name: 'arrow-swap',
            navigate: 'ListNomorRekening',
            iconType: Icon.Fontisto,
          },
          {
            item: 'Isi Surat',
            name: 'money-bill',
            navigate: 'IsiSurat',
            iconType: Icon.FontAwesome5,
          },
          {
            item: 'Tagihan',
            name: 'money-bill',
            navigate: 'ListTagihan',
            iconType: Icon.FontAwesome5,
          },
          {
            item: 'Upload Lainnya',
            name: 'qrcode',
            navigate: 'ListUploadd',
            iconType: Icon.FontAwesome,
          },
        ],
        headItemdua: [
          {
            item: 'Bukti Transfer',
            name: 'payment',
            navigate: 'ListUpload',
            iconType: Icon.MaterialIcons,
          },
          {
            item: 'Tagihan Pending',
            name: 'arrows-expand',
            navigate: 'ListUploadPending',
            iconType: Icon.Foundation,
          },
          {
            item: 'Tagihan Manual',
            name: 'account-settings-outline',
            navigate: 'UploadBuktiManual',
            iconType: Icon.MaterialCommunityIcons,
          },
        ],
      });
    } else {
      this.setState({
        headItem: [
          {
            item: 'Transfer',
            name: 'cloudupload',
            navigate: 'UploadBukti',
            iconType: Icon.AntDesign,
          },
          {
            item: 'Tagihan',
            name: 'money-bill',
            navigate: 'ListTagihan',
            iconType: Icon.FontAwesome5,
          },
          {
            item: 'Bukti Transfer',
            name: 'money-bill',
            navigate: 'ListUpload',
            iconType: Icon.FontAwesome5,
          },
          {
            item: 'Upload Lainnya',
            name: 'qrcode',
            navigate: 'ListUploadd',
            iconType: Icon.FontAwesome,
          },
        ],
        headItemdua: [],
      });
    }
    this.setState({animating: false});
  }

  render() {
    const {navigation} = this.props;

    return (
      // header
      <View style={styles.container}>
        {this.state.animating == true ? (
          <View>
            <Loading />
          </View>
        ) : (
          <View style={styles.container}>
            <View
              style={{
                height: hp('15%'),
                width: wp('100%'),
                borderBottomRightRadius: hp('25%'),
                backgroundColor: colors.colorstiga,
                flexDirection: 'row',
              }}>
              {this.props.infoUser.profile.foto == null ? (
                <Image
                  source={{
                    uri:
                      'https://png.pngtree.com/element_our/png_detail/20181102/avatar-profile-logo-vector-emblem-illustration-modern-illustration-png_227486.jpg',
                  }}
                  style={styles.imageProfile}
                />
              ) : (
                <Image
                  source={{
                    uri: url + this.props.infoUser.profile.foto,
                  }}
                  style={styles.imageProfile}
                />
              )}
              <View>
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    marginTop: hp('3%'),
                    fontSize: wp('5%'),
                    fontWeight: 'bold',
                  }}>
                  {this.props.infoUser.name}
                </Text>
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    // marginTop: hp('1%'),
                    // fontWeight: 'bold',
                    fontSize: wp('4%'),
                  }}>
                  {this.props.infoUser.profile.role}
                </Text>
              </View>
            </View>

            <View style={styles.containerIconGrid}>
              <View style={styles.subContainerIconGrid}>
                {this.state.headItem.map((item, key) => {
                  return (
                    <TouchableOpacity
                      onPress={() => navigation.navigate(item.navigate)}
                      style={styles.componentGridIcon}
                      key={key}>
                      <View style={styles.subComponentGridCyrcle}>
                        <item.iconType
                          name={item.name}
                          color={colors.colorssatu}
                          size={wp('6%')}
                        />
                      </View>
                      <Text
                        numberOfLines={1}
                        ellipsizeMode="tail"
                        style={styles.subComponentGridText}>
                        {item.item}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
            {this.props.infoUser.profile == null ? (
              ''
            ) : this.props.infoUser.profile.role == 'RT' ? (
              <View style={styles.containerIconGriddua}>
                <View style={styles.subContainerIconGrid}>
                  {this.state.headItemdua.map((item, key) => {
                    return (
                      <TouchableOpacity
                        onPress={() => navigation.navigate(item.navigate)}
                        style={styles.componentGridIcon}
                        key={key}>
                        <View style={styles.subComponentGridCyrcle}>
                          <item.iconType
                            name={item.name}
                            color={colors.colorssatu}
                            size={wp('6%')}
                          />
                        </View>
                        <Text
                          numberOfLines={1}
                          ellipsizeMode="tail"
                          style={styles.subComponentGridText}>
                          {item.item}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </View>
            ) : null}
          </View>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.YellowButton,
    alignItems: 'center',
  },
  imageProfile: {
    marginTop: hp('1%'),
    marginLeft: wp('2%'),
    width: wp('20'),
    height: wp('20'),
    borderRadius: wp('20'),
    borderWidth: 1,
  },
  containerIconGrid: {
    width: wp('95%'),
    backgroundColor: colors.colorstiga,
    borderRadius: 10,
    borderTopLeftRadius: wp('10%'),
    borderTopRightRadius: wp('10%'),
    alignItems: 'center',
    marginTop: hp('2%'),
  },
  containerIconGriddua: {
    width: wp('95%'),
    backgroundColor: colors.colorstiga,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: hp('1%'),
  },
  subContainerIconGrid: {
    margin: wp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp('90%'),
  },
  componentGridIcon: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp('20%'),
  },
  subComponentGridCyrcle: {
    borderRadius: wp('20%'),
    backgroundColor: 'white',
    height: wp('15%'),
    width: wp('15%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  subComponentGridText: {
    marginTop: 1,
    fontFamily: 'ProximaNova',
    fontSize: wp('3%'),
    color: colors.colorssatu,
    width: wp('22%'),
    alignItems: 'center',
    textAlign: 'center',
  },
  containercard: {
    width: wp('90%'),
    marginTop: hp('1%'),
  },
  textcategory: {
    fontSize: wp('5%'),
    fontWeight: 'bold',
  },
  subcontainercard: {
    width: wp('90%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  card: {
    marginLeft: wp('2%'),
    marginTop: hp('2%'),
    width: wp('45%'),
    height: hp('35%'),
    borderRadius: wp('3%'),
    borderColor: colors.colorstiga,
    borderWidth: 1,
    backgroundColor: 'white',
  },
  imgcard: {
    width: wp('45%'),
    height: wp('40%'),
    borderTopLeftRadius: wp('3%'),
    borderTopRightRadius: wp('3%'),
    borderWidth: 1,
  },
  judulcard: {
    marginLeft: wp('2%'),
    marginTop: hp('1%'),
    fontWeight: 'bold',
  },
  iconcomment: {
    marginTop: hp('3%'),
    width: wp('40%'),
    alignItems: 'flex-end',
  },
});

// export default home;
const mapStateToProps = ({AuthReducer, network}) => {
  const {token, infoUser} = AuthReducer;
  const {isConnected} = network;
  return {
    isConnected,
    token,
    infoUser,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dataUser,
      isLogged,
    },
    dispatch,
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(History);
