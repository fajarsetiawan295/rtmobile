import {TYPES} from '../types';

export const isLogged = value => {
  return {
    type: TYPES.LOGGED,
    payload: value,
  };
};

export const dataUser = value => {
  return {
    type: TYPES.USER,
    payload: value,
  };
};

export const tokenUser = value => {
  return {
    type: TYPES.TOKEN,
    payload: value,
  };
};
export const tokenfcm = value => {
  return {
    type: TYPES.TOKENFCM,
    payload: value,
  };
};

export const welcomeScreen = value => {
  return {
    type: TYPES.WELCOME,
    payload: value,
  };
};
