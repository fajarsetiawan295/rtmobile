import MaterialCommunityIconsI from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIconsI from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIconsI from 'react-native-vector-icons/MaterialIcons';
import FontAwesomeI5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesomeI from 'react-native-vector-icons/FontAwesome';
import FoundationI from 'react-native-vector-icons/Foundation';
import EvilIconsI from 'react-native-vector-icons/EvilIcons';
import OcticonsI from 'react-native-vector-icons/Octicons';
import IoniconsI from 'react-native-vector-icons/Ionicons';
import Fontist from 'react-native-vector-icons/Fontisto';
import FeatherI from 'react-native-vector-icons/Feather';
import EntypoI from 'react-native-vector-icons/Entypo';
import ZocialI from 'react-native-vector-icons/Zocial';
import AntDesignn from 'react-native-vector-icons/AntDesign';
import React from 'react';

const MaterialCommunityIcons = props => <MaterialCommunityIconsI {...props} />;
const SimpleLineIcons = props => <SimpleLineIconsI {...props} />;
const MaterialIcons = props => <MaterialIconsI {...props} />;
const FontAwesome5 = props => <FontAwesomeI5 {...props} />;
const FontAwesome = props => <FontAwesomeI {...props} />;
const Foundation = props => <FoundationI {...props} />;
const EvilIcons = props => <EvilIconsI {...props} />;
const Ionicons = props => <IoniconsI {...props} />;
const Octicons = props => <OcticonsI {...props} />;
const Fontisto = props => <Fontist {...props} />;
const Feather = props => <FeatherI {...props} />;
const Entypo = props => <EntypoI {...props} />;
const Zocial = props => <ZocialI {...props} />;
const AntDesign = props => <AntDesignn {...props} />;

export default {
  MaterialCommunityIcons,
  SimpleLineIcons,
  MaterialIcons,
  FontAwesome5,
  FontAwesome,
  Foundation,
  EvilIcons,
  Ionicons,
  Fontisto,
  Octicons,
  Feather,
  Entypo,
  Zocial,
  AntDesign,
};
