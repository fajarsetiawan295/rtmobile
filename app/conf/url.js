// export const url = 'http://192.168.1.8:8000';
export const url = 'https://rtandrw.herokuapp.com';

export const url_api = url + '/api';

export const api = {
  Login: url_api + '/auth/login',
  Register: url_api + '/auth/signup',
  Profile: url_api + '/auth/user',
  FCMTOKEN: url_api + '/auth/Create-FCM-Token',
  UpdateFoto: url_api + '/auth/UpdateFoto',
  ResetPassword: url_api + '/auth/Reset-Password',

  //   Active Warga
  ActiveWarga: url_api + '/auth/Cek-warga-RT-active',
  NonActiveWarga: url_api + '/auth/Cek-warga-RT-active',
  InputKey: url_api + '/auth/Create-Rt-warga',
  UpdateWarga: url_api + '/auth/Non-Active-Warga',

  // informasi
  CreateInformasi: url_api + '/Informasi/Create-Informasi',
  AllCategory: url_api + '/Informasi/lis-Categori',
  DetailsInformasi: url_api + '/Informasi/List-Informasi-Details/', //id
  Commentar: url_api + '/Informasi/komentar-Informasi',
  createdonasi: url_api + '/Informasi/Upload-Donasi',
  Listdonasi: url_api + '/Informasi/List-Donasi',
  Listinformasicategory: url_api + '/Informasi/List-Informasi-category/', //id

  // Tagihan
  CreateNomorRekening: url_api + '/Tagihan/Create-Rekening',
  CreateTagihan: url_api + '/Tagihan/Create-Tagihan',
  AllNomorRekening: url_api + '/Tagihan/List-Rekening',
  AllTagihan: url_api + '/Tagihan/List-Tagihan',
  UploadBuktiTagihan: url_api + '/Tagihan/Upload-Bukti-Pembayaran',
  TagihanUser: url_api + '/Tagihan/User-Upload/', //ada id nya
  TagihanUserPending: url_api + '/Tagihan/List-Tagihan-Upload/Pending',
  TagihanUserUpadte: url_api + '/Tagihan/Update-Status-Upload',
  UploadBuktiPembayaranManual:
    url_api + '/Tagihan/Upload-Bukti-Pembayaran-Manual',

  // Pengaduan
  ListRtPengaduan: url_api + '/Pengaduan/list-Pengauan-Rt',
  ListuserPengaduan: url_api + '/Pengaduan/list-Pengauan-user',
  CategoriPengaduan: url_api + '/Pengaduan/lis-Categori',
  PengaduanCreate: url_api + '/Pengaduan/Create-Pengaduan',
  PengaduanUPdate: url_api + '/Pengaduan/Update-Status',
  DetailsPengaduan: url_api + '/Pengaduan/Details-Pengaduan/', //ada id nya

  // surat
  CreateBodySurat: url_api + '/Surat/Body-Surat',
  ListJenisSurat: url_api + '/Surat/List-Jenis-Surat',
  PengajuanSurat: url_api + '/Surat/Pengajuan-Surat',
  GetPengajuanSurat: url_api + '/Surat/Get-Pengajuan-Surat',
  UpdateSurat: url_api + '/Surat/Update-Surat/', //ada id nya
  downloadsurat: url_api + '/auth/Cetak-Surat/', //ada id nya
};
