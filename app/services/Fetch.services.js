import axios from 'axios';

export const post_services = async (token, databody, url) => {
  console.log(url);
  console.log(databody);
  try {
    const asu = await axios.post(url, databody, {
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + token,
        key: 'qwertyuiop1234567890',
      },
    });
    return asu;
  } catch (err) {
    return err.response;
  }
};

export const Get_services = async (token, url) => {
  try {
    const asu = await axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
      },
    });
    return asu;
  } catch (err) {
    return err.response;
  }
};
